<?php

class Formulario {

    public $nombre = '';
    public $empresa = '';
    public $servicio = '';
    public $telefono = '';
    public $correo = '';
    public $fecha = '';
    public $status = '';

	public function Formulario(){
		$this->nombre = '';	
		$this->empresa = '';	
		$this->servicio = '';	
		$this->telefono = '';	
		$this->correo = '';	
		$this->fecha = '';	
		$this->status = '';	
	}

    public function setNombre($nombre){
      $this->nombre = $nombre;
    }
  	public function getNombre(){
      return $this->nombre;
    }
    public function setEmpresa($empresa){
      $this->empresa = $empresa;
    }
  	public function getEmpresa(){
      return $this->empresa;
    }
    public function setServicio($servicio){
      $this->servicio = $servicio;
    }
  	public function getServicio(){
      return $this->servicio;
    }
    public function setTelefono($telefono){
      $this->telefono = $telefono;
    }
  	public function getTelefono(){
      return $this->telefono;
    }
    public function setCorreo($correo){
      $this->correo = $correo;
    }
  	public function getCorreo(){
      return $this->correo;
    }
   	public function setFecha($fecha){
      $this->fecha = $fecha;
    }
  	public function getFecha(){
      return $this->fecha;
    }
    public function setStatus($status){
      $this->status = $status;
    }
  	public function getStatus(){
      return $this->status;
    }

    public function imprimir() {
			echo "" . $this->nombre . " | " . $this->empresa . " | " . $this->servicio . " | " . $this->telefono . " | " . $this->correo . " | " . $this->fecha . " | " . $this->status;
	}

    
}

?>